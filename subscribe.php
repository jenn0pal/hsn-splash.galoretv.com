<?php
/**
 * Created by PhpStorm.
 * User: Perseus
 * Date: 10/3/14
 * Time: 3:12 PM
 */


require_once 'mailer.php';

$file = 'assets/subscribers.csv';
$result = array();



$counter = 0;
$handle = fopen($file, "r");
while(!feof($handle)){
    $line = fgets($handle);
    if($line == $_POST['email'])
    {$counter++; break;}
}

if($counter > 0)
    {
        $email = "\n" . $_POST['email'];
        $result = array(
            'status' => 'Error',
            'message' => 'Email already exist!'
        );
        header('Content Type: application/json');
        echo json_encode($result);
    }

    else if (isset($_POST['submit']) && isset($_POST['email']) && $_POST['email'] != '') {
        $email = "\n" . $_POST['email'];
        if(filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){

        if (file_put_contents($file, $email, FILE_APPEND | LOCK_EX) && sendNewsletter($_POST['email'])) {
            /* header('Location: http://' . $_SERVER['SERVER_NAME'] . '/success.html');*/
            $result = array(
                'status' => 'OK',
                'message' => 'We got you!'
            );
        }
        }
        else {
        $result = array(
            'status' => 'Error',
            'message' => 'Ouch! There\'s something wrong with the server.'
        );
    }
    header('Content Type: application/json');
    echo json_encode($result);
}

